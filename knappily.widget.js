(function() {

    var jQuery;
    var skip = 0;
    var defaultLimit = 10;
    var section = 'weblist';
    var servicesPath = 'https://services.knappily.com/article/search';
    var articlesCount = 0;
    var reachedEnd = false;
    var heading = 'Current Affairs';
    var server_end = '';
    var setHeight = false;


    if (window.jQuery === undefined || window.jQuery.fn.jquery !== '1.4.2') {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type","text/javascript");
        script_tag.setAttribute("src",
            "http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js");
        if (script_tag.readyState) {
        script_tag.onreadystatechange = function () {
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                scriptLoadHandler();
            }
        };
        } else {
        script_tag.onload = scriptLoadHandler;
        }
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    } else {
        jQuery = window.jQuery;
        main();
    }


    function scriptLoadHandler() {
        jQuery = window.jQuery.noConflict(true);
        main();
    }

    function getPayload(){
        return {
            skip: skip,
            limit: defaultLimit,
            section: section,
        }
    }


    function loadData(){

        if(jQuery('.knappily-article-plugin__wrapper').scrollTop() > 100)
            jQuery('.knappily-scrolltp').show();
        else
            jQuery('.knappily-scrolltp').hide();
        if(jQuery('.knappily-article-plugin__wrapper').scrollTop() + jQuery('.knappily-article-plugin__wrapper').innerHeight() > jQuery('.knappily-article-plugin__wrapper')[0].scrollHeight - 500 && !reachedEnd) {
            skip += defaultLimit;
            skip = skip < articlesCount ? skip : articlesCount;
            var payload = getPayload()
            getArticles(payload);
            articlesCount === skip ? reachedEnd = true : null;
        }
    };

    function constructInitialDom(){
        var defaultColor = '#DC3023';
        var customColor = jQuery('#knappily-articles--sdk').attr("data-color");
        var appliedColor = customColor ? customColor : defaultColor;
        var scrollBarStyle = "<style type='text/css'>.knappily-article-plugin__wrapper::-webkit-scrollbar-thumb{background-color: "+appliedColor+"}</style>"; 
        jQuery(scrollBarStyle).appendTo('head');
        var articlesWarpper = '<div id="knappily-article-plugin__articles_wrapper"></div>';
        var knappilyArticlesWarpper = '<div class="knappily-article-plugin__wrapper"></div>';
        var closeElement = '<div class="knappily-article-plugin__header" style="background-color:'+appliedColor+'">\
            <div class="knappily-article-plugin__header__title">'+heading+'</div>\
        </div>';
        var scrollTop = '<div>\
            <div class="knappily-article-plugin__right__footer knappily-article-plugin__footer__arrow"></div>\
        </div>';
	var arrowImage = server_end+"chevron-up.png";
        var footer = '<div>\
            <div class="knappily-article-plugin__footer" style="background-color:'+appliedColor+'">\
                <a class="knappily-article-plugin__article" href="https://knappily.app.link/magazine_download" target="__blank">\
                    <p style="text-align:center;font-size:14px; margin-top:10px; color:white; text-decoration: underline;">For the best experience use <b>Knappily</b> app on your smartphone</p>\
                <a/>\
                <div class="knappily-scrolltp"><img src="'+arrowImage+'"/></div>\
            </div>\
        </div>'
         jQuery('#knappily-articles--sdk').append(footer+articlesWarpper);
         jQuery('.knappily-scrolltp').hide();
        jQuery('#knappily-article-plugin__articles_wrapper').append(closeElement);
        jQuery('#knappily-article-plugin__articles_wrapper').append(knappilyArticlesWarpper);
    }

    function main() {
        jQuery(document).ready(function($) {
            var css_link = $("<link>", {
                rel: "stylesheet",
                type: "text/css",
                href: server_end+"knappily.widget.css"
            });
            css_link.appendTo('head');
            constructInitialDom();
            jQuery('.knappily-scrolltp').click(function(){
                $('.knappily-article-plugin__wrapper').animate({scrollTop: '0px'}, 300);
            });
            var elementWidth = parseInt(jQuery('#knappily-articles--sdk').attr("data-width")) || "100%";
            var elementHeight = parseInt(jQuery('#knappily-articles--sdk').attr("data-height")) || "100%";
            var footerWidth = parseInt(jQuery('#knappily-articles--sdk').attr("data-width")) || 'calc(100%)';

            jQuery('#knappily-article-plugin__articles_wrapper').css({width: elementWidth, height: elementHeight});
            jQuery('#knappily-articles--sdk').css({width: elementWidth, height: elementHeight});
            jQuery('.knappily-article-plugin__footer').css({width: footerWidth});
            jQuery('.knappily-article-plugin__wrapper').scroll(loadData);
            var payload = getPayload();
            getArticles(payload);
        });
    }

    function getFormattedDescription(description){
        var length = 80;
        var ending = ' ...';
        if (description.length > length) {
            return description.split(" ").splice(0,20).join(" ") + ending;
        } else {
            return description;
        }
    }

    function getArticles(payload){
        jQuery.ajax({
            url: servicesPath,
            type: 'POST',
            dataType: 'json',
            data: payload,
            async: false,
            contentType: "application/x-www-form-urlencoded",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Basic a25hcHBpbHktYXV0aDpLbmEzOXBwaWx5IQ==")
            },
            complete: function(xhr) {
            },
            success: function(data) {
                if(!setHeight){
                    var TotalWidgetHeight = jQuery('#knappily-articles--sdk').outerHeight(true);
                    var wrapperHeight = TotalWidgetHeight - (85);
                    jQuery('.knappily-article-plugin__wrapper').css({height: wrapperHeight});
                    setHeight = true;
	        }
                articlesCount = data.count;
                var articles = data.articles.forEach(function(article){
                    var imageUrl = article.general.imageUrl;
                    var title = article.title;
                    var description = getFormattedDescription(article.general.text);
                    var articleId = article._id
                    var articleType = article.articleType
                    if(articleType !== 'QuoteCartoon'){
                        var articleElement = '<a href="http://knappily.com/article/'+articleId+'" target="_blank" class="knappily-article-plugin__article">\
                                                    <div class="knappily-article-plugin__article__wrapper">\
                                                    <div><img src="'+imageUrl+'" width="62px" height="75px" style="position:absolute;"/></div>\
                                                    <div class="knappily-article-plugin__item knappily-article-plugin__item__title">'+title+'</div>\
                                                    <div class="knappily-article-plugin__item knappily-article-plugin__item__desc">'+description+'</div>\
                                                </div>\
                                                </a>';
                        jQuery('.knappily-article-plugin__wrapper').append(articleElement);
                    }
                });
            }
    });
    }

})();
