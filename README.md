Usage:

1) include the script file `http://172.104.160.177/knappily_widget/knappily.widget.min.js` in the script tag.

2) add the following to render the widget `<div id="knappily-articles--sdk"></div>`.

Attributes:


    data-width

    data-height
    
    data-color


You can specify the width and height as per your requirement using the above attributes


Example:

`<div id="knappily-articles--sdk" data-width="400" data-height="400" data-color="#000000"></div>`